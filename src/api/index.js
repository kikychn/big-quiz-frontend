export const createPost = post =>
  fetch('/api/posts', {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(post)
  });

export const getPosts = () =>
  fetch('/api/posts')
    .then(response => response.json());

export const deletePost = id =>
  fetch(`/api/posts/${id}`, {
    method: 'DELETE',
  });
